/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitocode.tema2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author diego
 */
public class App {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
//        String texto = new String("MitoCode");
//        if (texto instanceof String ) {
//            System.out.println("Es un String");
//        }
//        
        Alumno al = new Alumno();
        if (al instanceof Persona) {
            System.out.println("Es una Persona");
        }
        
        System.out.println("Canasta abierta, porfavor ingrese Solo frutas");
     
        
        Manzana m1 = new Manzana("ROJA");
        Manzana m2 = new Manzana("VERDE");
        Naranja n1 = new Naranja("NARANJA SIN PEPA");
        Galleta g1 = new Galleta("CHOCOLATE");
        
        App app = new App();
        app.verificar(m1);
        app.verificar(m2);
        app.verificar(n1);
        app.verificar(g1);        
        
       
    }
         List canasta = new ArrayList();
     private void verificar(Object object){
         if (object instanceof Fruta) {
             canasta.add(object);
             System.out.println("Fruta agregada "+ ((Fruta)object).getNombre());
         }else{
             System.out.println("Elemento no permitido, solo frutas porfavor");
         }
     
     }
}
