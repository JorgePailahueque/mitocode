/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitocode.tema4;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author diego
 */
public class AppWild {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AppWild aw = new AppWild();
        
        Persona al1 = new Profesor("Mitocode");
        Persona al2 = new Profesor("Jorge");
        Persona al3 = new Profesor("Mito");
        List<Persona> lista = new ArrayList();
        lista.add(al1);
        lista.add(al2);
        lista.add(al3);
        aw.listarUpperBounded(lista);
        System.out.println("");
        aw.listarLowerBounded(lista);
        System.out.println("");
        aw.listarUnBounded(lista);
    }
    public void listarUpperBounded(List<? extends Persona>lista){
        for (Persona a:lista) {
            //if (a instanceof Alumno) {
                System.out.println(a.getNombre());
        
//            }else if(a instanceof Profesor){
//                System.out.println(((Profesor)a).getNombre());  
//            }
        }
    }
     public void listarLowerBounded(List<? super Alumno>lista){
        for (Object a:lista) {
            //if (a instanceof Alumno) {
                System.out.println(((Persona)a).getNombre());
        
//            }else if(a instanceof Profesor){
//                System.out.println(((Profesor)a).getNombre());  
//            }
        }
     }
        public void listarUnBounded(List<?>lista){
        for (Object al:lista) {
            //if (a instanceof Alumno) {
                System.out.println(((Persona)al).getNombre());
        
//            }else if(a instanceof Profesor){
//                System.out.println(((Profesor)a).getNombre());  
//            }
        }
    }
}
