/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitocode.tema6;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author diego
 */
public class AppSet {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Set<Persona> lista = new HashSet();
        lista.add(new Persona(1, "Mitocode", 2));
        lista.add(new Persona(1, "Code", 2));
        lista.add(new Persona(1, "Mito", 2));
        lista.add(new Persona(1, "Mitocode", 4));
        lista.add(new Persona(1, "Code", 2));
        lista.add(new Persona(1, "AAA", 2));
        //
     
        
        for (Persona per : lista) {
            System.out.println(per.getEdad()+"-"+per.getNombre());
        }
    
    }
    
}
