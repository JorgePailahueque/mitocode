/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitocode.tema6;

import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author diego
 */
public class AppTreeSet {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Set<Persona> lista = new TreeSet();
        lista.add(new Persona(1, "MitoCode", 22));
        lista.add(new Persona(2, "Code", 23));
        lista.add(new Persona(3, "Mito", 24));
        lista.add(new Persona(4, "Jorge", 25));
        lista.add(new Persona(3, "Mito", 24));
        lista.add(new Persona(6, "AAA", 27));
        
        
        for (Persona per : lista) {
            System.out.println(per.getId() + "-"+ per.getNombre()+"-"+per.getEdad());
        }
    }
    
}
