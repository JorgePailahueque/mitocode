/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitocode.tema6;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author diego
 */
public class AppLinkedHashSet {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Set<Persona> lista = new LinkedHashSet();
        lista.add(new Persona(1, "MitoCode", 22));
        lista.add(new Persona(1, "Mito", 22));
        lista.add(new Persona(1, "Code", 22));
        lista.add(new Persona(1, "Code", 22));
        lista.add(new Persona(1, "MitoCode", 25));
        lista.add(new Persona(1, "AAA", 22));
        
        for (Persona per : lista) {
            System.out.println(per.getEdad()+"-"+per.getNombre());
        }
        
    }
    
}
