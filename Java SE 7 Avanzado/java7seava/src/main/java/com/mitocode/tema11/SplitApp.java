/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitocode.tema11;

/**
 *
 * @author diego
 */
public class SplitApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String cadena ="Jorge|MitoCode|Mito|Code";
        String[]extraccion =cadena.split("\\|",5);
        
        for (String elemento : extraccion) {
            System.out.println(elemento);
        }
    }
    
}
