/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitocode.tema8;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 * @author diego
 */
public class QueueApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // TODO code application logic here
        Queue<Persona> cola = new PriorityQueue();
        cola.offer(new Persona(1,"MitoCode" , 28));
        cola.offer(new Persona(1,"MitoCode" , 27));
        cola.offer(new Persona(1,"MitoCode" , 25));
        cola.offer(new Persona(1,"MitoCode" , 26));
       
        
        
        
        while (!cola.isEmpty()) {
            System.out.println("Se procede a atender a "+cola.peek());
            System.out.println("Atendiendo a "+cola.poll());
            Thread.sleep(1000);
        }
    }
    
}
