/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitocode.tema8;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 *
 * @author diego
 */
public class DequeApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Deque<String> dq = new ArrayDeque();
        dq.add("1-Jorge");
        dq.add("2-Mito");
        dq.add("3-Code");
        dq.add(null);
        
        String x = dq.peek();
        System.out.println("Peek "+x);
        
        x= dq.poll();
        System.out.println("Poll "+x);
        x= dq.pop();
        System.out.println("Poop "+x);
    
        dq.addFirst("0-MitoCode");
        dq.addLast("4-JCode");
        
        
        for (String elemento :dq) {
            
        }
    }
    
}
