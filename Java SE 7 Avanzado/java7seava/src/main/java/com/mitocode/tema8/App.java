/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitocode.tema8;

import java.util.Stack;

/**
 *
 * @author diego
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // TODO code application logic here
        Stack<Persona> pila = new Stack();
        pila.push(new Persona(4, "MitoCode", 25));
        pila.push(new Persona(3, "MitoCode", 26));
        pila.push(new Persona(2, "MitoCode", 27));
        pila.push(new Persona(1, "MitoCode", 28));
     
        
        for (Persona elemento : pila) {
            System.out.println(elemento);
        }
        System.out.println("tope"+pila.peek());
        System.out.println("Search "+pila.search(new Persona(1, "MitoCode", 28)));
        
        System.out.println("LIFO");
        
        while(!pila.isEmpty()){
            System.out.println("Atendiendo a "+pila.pop());
            Thread.sleep(1000);
        } 
    }
    
}
