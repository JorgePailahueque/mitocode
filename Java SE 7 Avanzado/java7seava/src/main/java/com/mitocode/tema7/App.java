/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mitocode.tema7;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author diego
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Map<Persona,String> map = new HashMap();
        map.put(new Persona(1, "MitoCode", 21), "MitoCode");
        map.put(new Persona(2, "MitoCode", 22), "Mito");
        map.put(new Persona(2, "MitoCode", 22), "Mito");
        map.put(new Persona(5, "MitoCode", 25), "Jorge");
        map.put(new Persona(3, "MitoCode", 24), "AAA");
        map.put(new Persona(6, "MitoCode", 26), null);
        //map.put(null, "Hola");
        
//        Iterator it = map.keySet().iterator();
//        while(it.hasNext()){
//            Persona key = (Persona) it.next();
//            System.out.println("Clave: "+key+" -> Valor: "+map.get(key));
//        }
        for (Entry<Persona,String> ent : map.entrySet()) {
            System.out.println("Clave: "+ent.getKey()+" -> Valor: "+map.get(ent.getValue()));
            
        }
    }
    
}
